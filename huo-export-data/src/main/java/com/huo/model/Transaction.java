package com.huo.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String recordNo;
    private double pointEarned;
    private String poolId;
    private String corporateId;
    private String schemeId;
    private String transactionType;
    private String status;
    private String currencyCode;
    private int csn;
    private Date exportDate;

    @PrePersist
    public void setExportDate() {
        this.exportDate = new Date();
    }
}
