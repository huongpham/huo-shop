package com.huo.controller;

import com.huo.batch.configuration.BatchConfiguration;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/job")
public class JobController {

    @Autowired
    private JobOperator jobOperator;
    @Autowired
    private JobLauncher jobLauncher;

    @GetMapping(value = "/ping")
    public String ping() {
        return "Fine";
    }

    @GetMapping(value = "/export-trans")
    public String exportTransactionJob() throws Exception {
        this.jobOperator.start(BatchConfiguration.EXPORT_TRANSACTION_JOB,
                String.format("name=%s", System.currentTimeMillis()));
        return "OK";
    }

    @GetMapping(value = "/import-trans")
    public String importTransactionJob() throws Exception {
        this.jobOperator.start(BatchConfiguration.IMPORT_TRANSACTION_JOB,
                String.format("name=%s", System.currentTimeMillis()));
        return "OK";
    }
}
