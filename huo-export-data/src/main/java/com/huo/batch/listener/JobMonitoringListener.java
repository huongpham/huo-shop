package com.huo.batch.listener;

import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.*;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class JobMonitoringListener implements JobExecutionListener, StepExecutionListener {

    @Override
    public void beforeJob(JobExecution jobExecution) {
        log.debug("before job");
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        log.debug("after job");
    }

    @Override
    public void beforeStep(StepExecution stepExecution) {
        log.debug("beforeStep");
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        log.debug("afterStep");
        return null;
    }
}
