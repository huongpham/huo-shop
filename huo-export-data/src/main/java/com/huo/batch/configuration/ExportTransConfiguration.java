package com.huo.batch.configuration;

import com.huo.batch.processor.TransactionItemProcessor;
import com.huo.model.TransactionDTO;
import com.huo.util.FileUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.support.OraclePagingQueryProvider;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Configuration
@Slf4j
public class ExportTransConfiguration extends BatchConfiguration {

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Bean
    public TransactionItemProcessor processor(){
        return new TransactionItemProcessor();
    }
    @Bean
    public Job exportTransactionJob() {
        return jobBuilderFactory.get(EXPORT_TRANSACTION_JOB)
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(exportTransactionStep())
                .end()
                .build();
    }

    @Bean
    public Step exportTransactionStep() {
        return stepBuilderFactory.get("exportTransactionStep").<TransactionDTO, TransactionDTO>chunk(chunkSize)
                .reader(itemReaderStep())
                .processor(processor())
                .writer(transactionWriter())
                .faultTolerant()
                .skip(Exception.class)
                .build();
    }

    @Bean
    public ItemReader<TransactionDTO> itemReaderStep() {
        OraclePagingQueryProvider provider = new OraclePagingQueryProvider();
        provider.setSelectClause("t.*");
        provider.setFromClause("TRANSACTIONS t");
        provider.setWhereClause("t.status = 'A'");
        Map<String, Order> orderMap = new HashMap<>();
        orderMap.put("RECORD_NO", Order.ASCENDING);
        provider.setSortKeys(orderMap);

        JdbcPagingItemReader<TransactionDTO> reader = new JdbcPagingItemReader<>();
        reader.setDataSource(this.dataSource);
        reader.setFetchSize(chunkSize);
        reader.setPageSize(chunkSize);
        reader.setRowMapper(new BeanPropertyRowMapper<>(TransactionDTO.class));
        reader.setQueryProvider(provider);
        return reader;
    }

    @Bean
    public FlatFileItemWriter<TransactionDTO> transactionWriter() {
        FlatFileItemWriter<TransactionDTO> writer = new FlatFileItemWriter();
        String oFile = null;
        try {
            oFile = FileUtils.generateFile("output", "transactions.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        log.debug("file output: "+oFile);
        writer.setResource(new FileSystemResource(oFile));
        writer.setLineAggregator(new DelimitedLineAggregator<TransactionDTO>() {{
            setDelimiter(",");
            setFieldExtractor(new BeanWrapperFieldExtractor<TransactionDTO>() {{
                setNames(new String[] {"recordNo", "pointEarned", "poolId", "corporateId", "schemeId", "transactionType", "status", "currencyCode", "csn"});
            }});
        }});
        return writer;
    }

}
