package com.huo.batch.configuration;

import com.huo.batch.processor.ImportTransactionProcessor;
import com.huo.model.Transaction;
import com.huo.model.TransactionDTO;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;

import javax.sql.DataSource;

@Configuration
public class ImportTransConfiguration extends BatchConfiguration {

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    @Qualifier("secondDataSource")
    private DataSource secondDataSource;

    @Bean
    ImportTransactionProcessor importTransactionProcessor() {
        return new ImportTransactionProcessor();
    }
    @Bean
    public Job importTransactionJob() {
        return jobBuilderFactory.get(IMPORT_TRANSACTION_JOB).incrementer(new RunIdIncrementer())
                .listener(listener).flow(importTransactionStep()).end().build();
    }

    @Bean
    public Step importTransactionStep() {
        return stepBuilderFactory.get("importTransactionStep").<TransactionDTO, Transaction> chunk(chunkSize)
                .reader(importTransactionReader()).processor(importTransactionProcessor())
                .writer(importTransactionWriter())
                .faultTolerant()
                .skip(Exception.class)
                .build();
    }

    @Bean
    public ItemWriter<Transaction> importTransactionWriter() {
        return new JdbcBatchItemWriterBuilder<Transaction>()
                .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
                .sql(" INSERT INTO `transaction` (record_no, point_earned, pool_id, corporate_id, scheme_id, transaction_type, status, currency_code, csn, export_date)" +
                        " VALUES(:recordNo, :pointEarned, :poolId, :corporateId, :schemeId, :transactionType, :status, :currencyCode, :csn, :exportDate)")
                .dataSource(secondDataSource)
                .build();
    }

    @Bean
    public ItemReader<? extends TransactionDTO> importTransactionReader() {
        return new FlatFileItemReaderBuilder<TransactionDTO>()
                .name("importTransactionReader")
                .resource(new FileSystemResource("output/transactions.csv"))
                .delimited()
                .names(new String[]{"recordNo", "pointEarned", "poolId", "corporateId", "schemeId", "transactionType", "status", "currencyCode", "csn"})
                .fieldSetMapper(new BeanWrapperFieldSetMapper<TransactionDTO>(){ {
                    setTargetType(TransactionDTO.class);
                }
                }).build();
    }
}
