package com.huo.batch.processor;

import com.huo.model.Transaction;
import com.huo.model.TransactionDTO;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.BeanUtils;
import org.springframework.util.StringUtils;

public class ImportTransactionProcessor implements ItemProcessor<TransactionDTO, Transaction> {

    @Override
    public Transaction process(TransactionDTO input) throws Exception {
        Transaction trans = new Transaction();
        BeanUtils.copyProperties(input, trans);
        if (StringUtils.isEmpty(input.getPointEarned())) {
            trans.setPointEarned(0);
        }
        if (StringUtils.isEmpty(input.getCsn())) {
            trans.setCsn(0);
        }
        return trans;
    }
}
