package com.huo.batch.processor;

import com.huo.model.TransactionDTO;
import org.springframework.batch.item.ItemProcessor;

public class TransactionItemProcessor implements ItemProcessor<TransactionDTO, TransactionDTO> {
    @Override
    public TransactionDTO process(TransactionDTO transactionDTO) throws Exception {
        return transactionDTO;
    }
}
