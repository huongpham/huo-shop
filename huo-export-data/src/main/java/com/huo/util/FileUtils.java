package com.huo.util;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileUtils {

    public static final String DATE_FILE_NAME = "MMddyyyy-hhmmss";

    public static String generateFile(String sPath, String name) throws IOException {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FILE_NAME);
        String sFile = sPath + File.separator+ name.substring(0, name.indexOf("."))
                + format.format(new Date()) + name.substring(name.indexOf("."));
        File path = new File(sPath);
        if (!path.exists()) {
            path.mkdirs();
        }

        File file = new File(sFile);
        if (!file.exists()) {
            file.createNewFile();
        }
        return sFile;
    }
}
