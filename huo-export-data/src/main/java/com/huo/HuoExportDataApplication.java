package com.huo;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableBatchProcessing
public class HuoExportDataApplication {

    public static void main(String[] args) {
        SpringApplication.run(HuoExportDataApplication.class, args);
    }

}
